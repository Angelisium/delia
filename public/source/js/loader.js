function post(url, body) {
	return fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: body ? body : ""
	});
}

async function loadImage(url) {
	let myImage = new Image(),
		response = await fetch(url);
	if(response.ok) {
		myImage.src = URL.createObjectURL(await response.blob());
		return myImage;
	} return false;
}

function getNextURL(url) {
	return url.replace(/([0-9]+)$/, function(a) {
		let Nlen = a.length,
			Nint = parseInt(a) + 1,
			Ntxt = new Array(Nlen).fill('0');
		Ntxt.push(Nint);
		return Ntxt.join('').slice(0-Nlen);
	});
}

(async function() {
	let response = await post(window.location.href).then(rep => rep.json()),
		nextURL = getNextURL(window.location.href)
		hasNext = await post(nextURL, "exist").then(rep => rep.json()),
		rawsContainer = document.querySelector('#raws'),
		nextButton = document.querySelector('#next');

	nextButton.addEventListener('click', function(event) {
		if(hasNext) {
			window.location.href = nextURL;
		} else {
			window.location.href = window.location.href.replace(/-[0-9]+$/, "s");
		}
	});

	for(let raw of response.raws) {
		let image = await loadImage(raw);
		rawsContainer.appendChild(image);
	}
})();