<?php
	spl_autoload_register(function ($name) {
		$path = __ROOT__ . "/";
		if($name === "Core\\CoreManager") {
			$path .= "CoreManager.php";
		} else {
			$path .= $name . ".php";
		}
		$path = str_replace("\\", "/", $path);
		require_once($path);
	});
