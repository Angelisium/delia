<?php
	use Core\CoreManager;

	class Controller extends CoreManager {
		public function GET(String $URI, String $page):String {
			// return $this->render("view/$page");

			return $this->render("view/legi_sample", [
				'title' => [
					'cookies' => "Cookies, Balises Web et Technologies similaires",
					'legal-notices' => "Mentions Légales",
					'privacy' => "Politique de confidentialité",
					'terms-of-use' => "Conditions générales d'utilisation",
				][$page],
			]);
		}
	}
