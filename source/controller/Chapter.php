<?php
	use Core\CoreManager;

	use source\model\Raws;
	use source\model\Webtoon;

	class Controller extends CoreManager {
		public function GET(String $URI, String $webtoon, String $chapter):String {
			$WebtoonModel = $this->_em(Webtoon::class);

			// soon add 404 error for non-existent chapters 

			return $this->render("view/chapter", [
				'webtoon' => $WebtoonModel->findOneBySlug($webtoon),
				'chapter' => $chapter
			]);
		}

		public function POST(String $URI, String $webtoon, String $chapter):String {
			$this->ContentType = "application/json";

			$RawsModel = $this->_em(Raws::class);
			$BrutRaws =  $RawsModel->findAllByChapter($webtoon, $chapter);

			if(isset($_POST['exist'])) {
				return (count($BrutRaws) > 0) ? 'true' : 'false';
			}

			$Raws = [];
			foreach ($BrutRaws as $BrutRaw) {
				$Raws[] = "/" . implode("/", [
					"webtoon",	$BrutRaw["webtoon"],
					"chapitre",	$BrutRaw["chapter"],
					"raws",		$BrutRaw["slug"],
				]);
			}

			return json_encode([
				'code' => $this->response['code'],
				'description' => $this->response['desc'],
				'raws' => $Raws
			], JSON_PRETTY_PRINT);
		}
	}
