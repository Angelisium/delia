<?php
	use Core\CoreManager;

	use source\model\Webtoon;

	class Controller extends CoreManager {
		public function POST(String $URI):String {
			if(!isset($_POST['password'])
			|| !password_verify(
				$_POST['password'],
				$this->_ENV["ADMIN"]["PASSWORD"]
			)) {
				$this->response['code'] = 404;
				$this->response['desc'] = "Not Found";
				return $this->render("view/error/404");
			}

			$this->ContentType = "application/json";
			$WebtoonModel = $this->_em(Webtoon::class);
			$WebtoonModel->add($_POST["name"], $_POST["slug"], $_POST["synopsis"]);
			return json_encode([
				'code' => 200,
				'description' => "OK",
				'response' => "Le webtoon a été ajouté.",
			], JSON_FORCE_OBJECT | JSON_PRETTY_PRINT);
		}
	}
