<?php
	use Core\CoreManager;
	use source\model\Webtoon;

	class Controller extends CoreManager {
		public function GET(String $URI, String $page = "1"):String {

			/**
			 * Soon :
			 *  - Set up a paging system
			 *  - Rework L18-20, 404 management.
			 */

			$WebtoonModel = $this->_em(Webtoon::class);
			$offset = (intval($page) - 1) * 10;
			$webtoons = $WebtoonModel->findAll(10, $offset);

			if(count($webtoons) === 0) {
				return $this->render("view/error/404");
			}

			return $this->render("view/library", [
				'webtoons' => $webtoons,
			]);
		}
	}
