<?php
	use Core\CoreManager;
	use source\model\Webtoon;
	use source\model\Chapter;

	class Controller extends CoreManager {
		public function GET(String $URI, String $slug, String $page = "1"):String {
			$WebtoonModel = $this->_em(Webtoon::class);
			$ChapterModel = $this->_em(Chapter::class);

			return $this->render("view/book", [
				'webtoon' => $WebtoonModel->findOneBySlug($slug),
				'chapters' => $ChapterModel->findAllByWebtoon($slug)
			]);
		}
	}
