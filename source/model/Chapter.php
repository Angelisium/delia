<?php
	namespace source\model;

	use source\model\Model;
	use source\small\library\Sql;

	class Chapter extends Model {
		public function findAllByWebtoon(
			String $slug,
			Int $count = -1,
			Int $offset = 0
		):Array {
			$sql = new Sql($this->_prefix);
			$sql->select('*')
				->from('chapter')
				->where("`webtoon` = ?");
			if($count > 0) {
				$sql->limit($count)->offset($offset);
			}
			$rep = $this->prepare($sql, [$slug]);
			return $rep;
		}
	}
