<?php
	namespace source\model;

	use source\model\Model;
	use source\small\library\Sql;

	class Raws extends Model {
		public function findAllByChapter(
			String $webtoon_slug,
			String $chapter_slug
		):Array {
			$sql = new Sql($this->_prefix);
			$sql->select('*')
				->from('raw')
				->where("`webtoon` = ? AND `chapter` = ?");
			$rep = $this->prepare($sql, [
				$webtoon_slug,
				$chapter_slug,
			]);
			return $rep;
		}
	}
