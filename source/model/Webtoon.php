<?php
	namespace source\model;

	use source\model\Model;
	use source\small\library\Sql;

	class Webtoon extends Model {
		public function findOneBySlug(String $slug):Array|Null {
			$sql = new Sql($this->_prefix);
			$sql->select('*')
				->from('webtoon')
				->where("`slug` = ?");
			$rep = $this->prepare($sql, [$slug]);
			return isset($rep[0]) ? $rep[0] : null;
		}

		public function findAll(Int $count = -1, Int $offset = 0) {
			$sql = new Sql($this->_prefix);
			$sql->select('*')
				->from('webtoon');
			if($count > 0) {
				$sql->limit($count)->offset($offset);
			}
			$rep = $this->queryAll($sql);
			return $rep;
		}

		public function add(String $name, String $slug, String $synopsis):Bool {
			$sql = new Sql($this->_prefix);
			$sql->insert('webtoon')
				->column('name', 'slug', 'synopsis')
				->values('?', '?', '?');
			$rep = $this->prepare($sql, [
				$name,
				$slug,
				$synopsis
			]);
			return true;
		}
	}
