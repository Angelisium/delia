<?php
	namespace source\model;

	abstract class Model {
		protected function query(string $sql) {
			$req = $this->_pdo->query($sql);
			$rep = $req->fetch();
			$req->closeCursor();
			return $rep;
		}
		protected function queryAll(string $sql) {
			$req = $this->_pdo->query($sql);
			$rep = $req->fetchAll();
			$req->closeCursor();
			return $rep;
		}
		protected function prepare(string $sql, array $param) {
			$req = $this->_pdo->prepare($sql);
			$rep = $req->execute($param);
			$rep = $req->fetchAll();
			$req->closeCursor();
			return $rep;
		}
	}
