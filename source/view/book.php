<?php

	$extends = "view/index"; 

	$title = " - " . $global['webtoon']['name'];

ob_start(null); ?>
				<img style="max-width:100%;" src="/webtoon/<?= $global['webtoon']['slug'] ?>/cover.jpg">
				<div class="info">
					<div class="name"><?= $global['webtoon']['name'] ?></div>
					<div class="syno"><?= $global['webtoon']['synopsis'] ?></div>
				</div>
<?php
	if(isset($global['chapters']) && count($global['chapters']) > 0) { 
		foreach($global['chapters'] as $chapter) {
			$img = "/webtoon/" . $chapter['webtoon'] . "/chapitre";
			$url = "$img-" . $chapter['slug'];
			$img = "$img/" . $chapter['slug'] . "/cover.jpg";
?>
				<a class="book" href="<?=$url?>">
					<img src="<?=$img?>" alt="Chapitre <?=$chapter['slug']?>">
					<div class="title">Chapitre N°<?=$chapter['slug']?></div>
				</a>
<?php
		}
	}

	$content = ob_get_clean();
