<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DeliA - Chapitre <?= $global['chapter'] ?> de <?= $global['webtoon']['name'] ?>.</title>
		<link rel="stylesheet" href="/source/css/loader.css">
	</head>
	<body>
		<main>
			<h1>
				<a href="/webtoon/<?=$global['webtoon']['slug']?>/chapitres">
					<?= $global['webtoon']['name'] ?>
				</a>
			</h1>
			<div id="raws"></div>
			<a id="next">Fin du chapitre - Voir le suivant</a>
		</main>
		<script src="/source/js/loader.js"></script>
	</body>
</html>
