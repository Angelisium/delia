<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DeliA<?= $title ?></title>
		<link rel="stylesheet" href="/source/css/normalize.css">
		<link rel="stylesheet" href="/source/css/style.css">
	</head>
	<body>
		<header>
			<nav>
				<a class="logo" href="/">DeliA</a>
				<!--<div>
					<a href="/webtoon/">Catalogue</a>
					<a href="/">À Propos</a>
				</div>-->
			</nav>
		</header>
		<main>
			<article>
<?= $content ?>
			</article>
			<footer>
				<p>Copyright 2022 - ANGELISIUM</p>
				<ul>
					<li><a href="/legal-notices">Mentions Légales</a></li>
					<li><a href="/terms-of-use">Conditions générales d'utilisation</a></li>
					<li><a href="/privacy">Politique de confidentialité</a></li>
					<li><a href="/cookies">Politique des Cookies</a></li>
				</ul>
			</footer>
		</main>
	</body>
</html>
