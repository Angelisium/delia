<?php

	$extends = "view/index"; 

	$title = " - 404 Not Found";

ob_start(null); ?>
				<div class="content">
					<h1>Not Found</h1>
					<p>The requested URL was not found on this server.</p>
				</div>
<?php

	$content = ob_get_clean();