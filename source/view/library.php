<?php

	$extends = "view/index"; 

	$title = " - Webtoons";

	ob_start(null);
	
	foreach($global['webtoons'] as $webtoon) { ?>
				<a class="book" href="/webtoon/<?= $webtoon['slug'] ?>/chapitres">
					<img src="/webtoon/<?= $webtoon['slug'] ?>/scover.png" alt="<?= $webtoon['name'] ?>">
					<div class="title" ><?= $webtoon['name'] ?></div>
				</a>
<?php
	}

	$content = ob_get_clean();
