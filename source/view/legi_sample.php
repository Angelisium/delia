<?php

	$extends = "view/index"; 

	$title = " - " . $global['title'];

ob_start(null); ?>
				<div class="content">
					<h1><?= $global['title'] ?></h1>
					<p>Premier pavé. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aperiam sunt molestias maiores fuga quia nostrum voluptatem voluptates, quidem, velit, nesciunt inventore eius totam autem distinctio nobis? Reprehenderit, explicabo aperiam. At sapiente rerum, nisi eum praesentium sint sequi nemo quisquam, ab id, aspernatur commodi facilis? Facilis necessitatibus eius nisi earum blanditiis recusandae rerum incidunt error modi harum, deleniti ducimus sunt dolorum nemo velit. Voluptatem, ea dolor? Cumque doloribus porro repudiandae similique, velit libero, obcaecati placeat cupiditate blanditiis vel non commodi rerum optio omnis facilis inventore. Optio quae hic necessitatibus labore possimus exercitationem, quaerat facilis unde, in iure, inventore ab iste aperiam!</p>
					<h2>1. Un sous-titre</h2>
					<p>Dans lequel il y a un aute pavé. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Perferendis facere ad, sunt nostrum magni explicabo provident praesentium magnam eum. Temporibus provident officia pariatur possimus repudiandae accusamus odio sequi ut laudantium, consectetur voluptas ab, dolor soluta non consequatur, ad doloribus maiores aliquam corporis eligendi error tempore. Repellat ipsum officiis, ea quibusdam, doloremque accusantium dolorum dignissimos nostrum, adipisci ullam assumenda? Ut, quos officiis. Aliquid quos explicabo harum, facere quasi debitis sequi possimus commodi. Eaque blanditiis quaerat neque!</p>
					<h2>2. Un autre sous-titre</h2>
					<h3>2.1 Un sous-sous-titre</h3>
					<p>Pavé César ! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatem rem maiores, laborum corporis autem omnis doloremque suscipit! Totam labore natus qui explicabo eius culpa assumenda odio voluptatibus corporis quia, ab, molestias quod voluptatum necessitatibus vitae?</p>
					<p>Avec une petite liste, pour la forme.</p>
					<ul>
						<li>Les données personnelles que nous recueillerons</li>
						<li>L’utilisation des données recueillies</li>
						<li>Qui a accès aux données recueillies</li>
						<li>Les droits des utilisateurs du site</li>
						<li>La politique de cookies du site</li>
					</ul>
					<p>blabla Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolores tempora suscipit soluta natus commodi ipsum? Et hic non maiores necessitatibus doloremque excepturi error unde magnam maxime ea enim rem autem veritatis saepe nemo a deleniti cupiditate beatae ipsum alias sint, iste quos reprehenderit quas. Provident repudiandae eligendi natus, debitis totam minus cum, aspernatur autem culpa deleniti quis officiis assumenda. Voluptatibus, nostrum consequatur aliquid rerum labore quaerat tenetur sint blanditiis, ea nulla, iste sunt saepe laudantium.</p>
					<h3>2.2 Un autre sous-sous-titre, pour la forme.</h3>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure animi deleniti fugit consectetur eligendi quidem amet illo quibusdam explicabo sunt quae eos aperiam, repellendus error alias corporis quas. Neque, provident labore! Qui, vel ipsum incidunt inventore odio facere architecto laudantium ipsam veniam, tenetur eius quo reprehenderit perspiciatis adipisci culpa unde cupiditate doloremque maiores cum error iusto deleniti quibusdam minima. Numquam eveniet omnis temporibus ratione quibusdam totam dolorem iste repellendus doloribus repellat. Aliquid temporibus sequi, veritatis assumenda dolore hic at earum nulla! Perspiciatis quo tempora illum! Est, magnam. Fuga, quasi aliquam!</p>
					<p>Et voilà, c'est tout pour le blabla &lt;3</p>
				</div>
<?php

	$content = ob_get_clean();
