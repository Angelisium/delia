## Deployment
Just clone it, create db, edit config.ini and enjoy. (more details soon)

## How to add a book:
The first step is to add the graphic elements that compose this book according to the following architecture:
```
└── root/
	└── public/
		└── webtoon/
			└── book-slug/
				├── chapitre/
				│	├── chapitre-slug/
				│	│	├── raws/
				│	│	│	├── file-1-slug.jpg
				│	│	│	├── file-2-slug.jpg
				│	│	│	└── # etc
				│	│	└── cover.jpg
				│	└── # chapitre-2-slug, etc
				├── scover.png
				└── cover.jpg
```
Then, add in table `webtoon` the book info (book-slug, the name and the synopsis).
Then, add in table `chapter` all chapter info (book-slug, chapitre-slug).
Then, add in table `raw` all raws (book-slug, chapter-slug, file-1-slug.jpg).

Soon, an administration page to quickly add books, chapters and raws.

## Soon :
this improvement is not planned for now and may never be implemented because this tool is a reader that I use for my personal use.
 - Create an ini.php file to execute to create the tables (with the prefix set in `config.ini`) in the database.
	```sql
	CREATE TABLE `webtoon` (
		`slug` VARCHAR(80) NOT NULL,
		`name` VARCHAR(80) NOT NULL,
		`synopsis` TEXT NOT NULL,
		PRIMARY KEY (`slug`)
	) ENGINE = InnoDB;

	CREATE TABLE `author` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(80) NOT NULL,
		PRIMARY KEY (`id`)
	) ENGINE = InnoDB;

	CREATE TABLE `credit` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`author` INT NOT NULL,
		`webtoon` VARCHAR(80) NOT NULL,
		PRIMARY KEY (`id`),
		CONSTRAINT FK_author_webtoon FOREIGN KEY (`author`) REFERENCES `author`(`id`),
		CONSTRAINT FK_webtoon_author FOREIGN KEY (`webtoon`) REFERENCES `webtoon`(`slug`)
	) ENGINE = InnoDB;

	CREATE TABLE `chapter` (
		`webtoon` VARCHAR(80) NOT NULL,
		`slug` VARCHAR(7) NOT NULL,
		PRIMARY KEY (`webtoon`, `slug`),
		CONSTRAINT FK_webtoon_chapter FOREIGN KEY (`webtoon`) REFERENCES `webtoon`(`slug`)
	) ENGINE = InnoDB;

	CREATE TABLE `raw` (
		`webtoon` VARCHAR(80) NOT NULL,
		`chapter` VARCHAR(7) NOT NULL,
		`slug` VARCHAR(7) NOT NULL,
		PRIMARY KEY (`webtoon`, `chapter`, `slug`),
		CONSTRAINT FK_chapter_raw FOREIGN KEY (`webtoon`, `chapter`) REFERENCES `chapter`(`webtoon`, `slug`)
	) ENGINE = InnoDB;
	```
 - make an admin panel to easily add book, chapter and raws.
 - add in `source/routes.regex` + pagination system
	```
	|(?:/webtoons/page-([0-9]+))(*MARK:Library)
	|(?:/webtoon/([0-9a-z\-]+)/chapitres/page-([0-9]+))(*MARK:Book)
	```
 - rework this readme.md
 - add samples of books in the public domain (e.g. [Airlock Bound](https://www.webtoons.com/en/challenge/airlock-bound-the-public-domain-web-comic/list?title_no=739919), [Pepper&Carrot](http://www.peppercarrot.com/fr/), etc)
